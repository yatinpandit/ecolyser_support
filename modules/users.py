from flask import Flask, Blueprint,request,jsonify
import time
from datetime import datetime
import requests
import json

users_blueprint = Blueprint("users",__name__)


def logs_api(log):
    url = 'http://0.0.0.0:5060/log/register'
    api_status = requests.post(url, data = json.dumps(log))


@users_blueprint.route('/user/login',methods=['POST'])
def login():
    try:
        log = {}
        now = datetime.now()
        timestamp = datetime.timestamp(now)
        log['time'] = int(timestamp)
        raw_data = json.loads(request.data)
        log['raw data'] = raw_data
        if raw_data['username'] == 'test' and raw_data['password'] == "test":
            log['return'] = {"errCode":1,"Status": 'Success'}
            log['id'] = "1234"
            logs_api(log)
            return jsonify({"errCode":1,"Status": 'Success'})
        else:
            raise Exception("incorrect password")
    except Exception as e:
        log["return"] = {"errCode":-1,"Status": str(e)}
        log['id'] = "1234"
        logs_api(log)
        return {"errCode": -1,"status ":str(e)}


    


