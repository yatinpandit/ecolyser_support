from flask import Flask, Blueprint, flash, redirect, request,jsonify
import os
import json
from .helpers import *

logs_blueprint = Blueprint('logs_store',__name__)


@logs_blueprint.route("/log/register",methods=['POST'])
def register_logs():
    try:
        raw_data = json.loads(request.data)
        # if not raw_data['id'] or not raw_data['action'] or not raw_data['time']:
        #     raise Exception("Incomplete Data")
        with open("logs/"+raw_data['id']+'_'+str(raw_data['time']), "w")  as filewrite :
            filewrite.write(json.dumps(raw_data))
        return jsonify({"errCode":1,"status":"Success"})
    except Exception as e:
        print(str(e))
        return jsonify({"errCode":-1,"status":str(e)})