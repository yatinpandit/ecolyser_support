import pymongo
from bson.objectid import ObjectId
from copy import deepcopy

def connectionfunction(collection_name,database="logs"):
        client = pymongo.MongoClient(host="localhost",port=27017)
        db = client[database]
        collection = db[collection_name]
        return collection

def insert_data(collection_name,data):
    collections = connectionfunction(collection_name)
    myquery = { "_id": ObjectId(data['id'])}
    return_status = collections.find(myquery).count()
    if return_status == 0 or return_status == None:
        return_id = collections.insert(data)
        return str(return_id)
    else:
        collections.update_one(myquery,{'$push':{'actions':data['action']}})
        return None
    