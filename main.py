from flask import Flask, Blueprint, flash, redirect, request
import os
from modules.users import users_blueprint
from modules.logs_store import logs_blueprint

app = Flask(__name__)

app.register_blueprint(users_blueprint)
app.register_blueprint(logs_blueprint)


if __name__ == "__main__":
    app.secret_key = os.urandom(12)
    app.run(debug=True,host="0.0.0.0",port="5060")